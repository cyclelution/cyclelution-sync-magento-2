<?php
namespace Cyclelution\Sync\Controller\Index;

//use Magento\Framework\Setup\ModuleDataSetupInterface;

class Syncorderstatus extends \Magento\Framework\App\Action\Action
{
  public function __construct(
		\Magento\Framework\App\Action\Context $context)
  {
    return parent::__construct($context);
  }


  public function execute()
  {

    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    
    $logger = $objectManager->get('Psr\Log\LoggerInterface');

    $logger->debug('running order sync');

    $_order = $objectManager->create('Magento\Sales\Model\Order');

    $helper = $objectManager->create('Cyclelution\Sync\Helper\Data');
    $cyc_userid = $helper->getGeneralConfig('cyc_userid');
    $cyc_api = $helper->getGeneralConfig('cyc_api');
    $cyc_token = $helper->getGeneralConfig('cyc_token');

    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'running order sync'");

    $current_list = $resource->getConnection()->fetchAll("SELECT cyc_sync_log_id, cyc_sync_log_magento_order_id, cyc_sync_log_cyc_order_id  FROM cyc_sync_log where cyc_sync_log_text = 'AfterPlaceOrder' and cyc_sync_log_status = 1;");

    if(count($current_list) > 0){

      foreach($current_list as $row){

        $id = $row['cyc_sync_log_id'];
        $magento_order_id = $row['cyc_sync_log_magento_order_id'];
        $cyc_order_id = $row['cyc_sync_log_cyc_order_id'];
        $override_text = '';

        $logger->debug("testing order $cyc_order_id");

        $api_export_url = "https://$cyc_api/CyclelutionWebAPI/api/CyclelutionInventory/ ter?ShipmentHeaderID=$cyc_order_id";


        //post to API
        $ch = curl_init($api_export_url);
        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          //'Content-Length: ' . strlen($postData),
          'Authorization: BasicAuth ' .$cyc_token,
          'Content-Type: application/json'
          )
        );
        
        $response = json_decode(curl_exec($ch), true);

        $logger->debug("Result" . json_encode($response));

        if($response['Status'] == false){

          //failed
          $logger->debug("testing order $cyc_order_id");

        } else {

          //capture order ID and store in db
          $StatusID = intval($response['Result']['StatusID']);
          $Status = $response['Result']['Status'];
          $ShipmentMethod = $response['Result']['ShipmentMethod'];
          $TrackingNumber = $response['Result']['TrackingNumber'];
          $PackingTime = $response['Result']['PackingTime'];

          $logger->debug("order StatusID$StatusID Status$Status ShipmentMethod$ShipmentMethod TrackingNumber$TrackingNumber PackingTime$PackingTime");

          //if tracking# is not null then it was shipped?
          
          //update magento order
          $order = $_order->load($magento_order_id);

          if (! $order->canShip()) {
            $logger->debug("order cant ship");
            $resource->getConnection()->query("UPDATE cyc_sync_log SET cyc_sync_log_status = 2 where cyc_sync_log_id = $id");
            continue;
          }
           
          $convertOrder = $objectManager->create('Magento\Sales\Model\Convert\Order');

          $shipment = $convertOrder->toShipment($order);

          //create shipment
          foreach ($order->getAllItems() AS $orderItem) {
              // Check if order item has qty to ship or is virtual
              if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                  continue;
              }

              $qtyShipped = $orderItem->getQtyToShip();

              // Create shipment item with qty
              $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

              // Add shipment item to shipment
              $shipment->addItem($shipmentItem);
          }

          // Register shipment
          $shipment->register();

          $shipment->getOrder()->setIsInProcess(true);

          

          try {
              // Save created shipment and order
              $shipment->save();
              $shipment->getOrder()->save();

              $data = array(
                  'carrier_code' => 'ups',
                  'title' => $ShipmentMethod,
                  'number' => 'TORD23254WERZXd3',
              );

              $TrackingNumber = 'ABC123';

              $track = $objectManager->create('Magento\Sales\Model\Order\Shipment\Track')
              ->setShipment($shipment)
              ->setTitle($ShipmentMethod)
              ->setNumber($TrackingNumber)
              ->setCarrierCode($ShipmentMethod)
              ->setOrderId($shipment->getData('order_id'))
              ->save();

              //$shipment->addTrack($track)->save();

              // Send email
              $objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                  ->notify($shipment);

              $shipment->save();

              //get shipment_id
              $shipment_id = $shipment->getShipmentId();

              $logger->debug("shipment saved shipment_id$shipment_id");

              $resource->getConnection()->query("UPDATE cyc_sync_log SET cyc_sync_log_status = 2 where cyc_sync_log_id = $id");

          } catch (\Exception $e) {

              $logger->debug("order error " . $e->getMessage());

          }

          //update db



        }

      }

    } else {
      $logger->debug('no order to sync');
    }

    exit;
  }
}