<?php
namespace Cyclelution\Sync\Controller\Index;

//use Magento\Framework\Setup\ModuleDataSetupInterface;

class Sync extends \Magento\Framework\App\Action\Action
{
  public function __construct(
		\Magento\Framework\App\Action\Context $context)
  {
    return parent::__construct($context);
  }


  public function execute()
  {
    //echo 'Sync';

    //link to api
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    
    $logger = $objectManager->get('Psr\Log\LoggerInterface');

    $logger->debug('running sync');

    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'running sync'");

	//before we start lets grab all products that has Cyc_item = 1
	$current_list = $resource->getConnection()->fetchAll("SELECT cyc_sync_log_id, cyc_sync_log_int, cyc_sync_log_sku  FROM cyc_sync_log where cyc_sync_log_sku is not null and cyc_sync_log_status = 1 and cyc_sync_log_int > 0;");

	//var_dump($current_list);

	$logger->debug('wipe current inventory');
	if(count($current_list) > 0){

		foreach($current_list as $row){

			$id = $row['cyc_sync_log_id'];
			$override_text = '';

			//update each product stock 0 and disable inventory
			$stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
			
			$stockItem = $stockRegistry->getStockItem($row['cyc_sync_log_int']);

			//$product_temp = $objectManager->get('Magento\Catalog\Model\Product')->load($row['cyc_sync_log_int']);

			//$temp1 = json_encode($product_temp->getName());
			$p_id_integrity_check = $stockItem->getQty();

			if($p_id_integrity_check == '' || $p_id_integrity_check == null){

				$override_text = 'p_id missing';

				//$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = '$sku p_id missing'");


			} else {

				$stockItem->setData('qty',0); //set updated quantity
				//$stockItem->setData('manage_stock',$stockData['manage_stock']);
		        $stockItem->setData('is_in_stock',0);
		        //$stockItem->setData('use_config_notify_stock_qty',1);
				//$stockRegistry->updateStockItemById($row['cyc_sync_log_sku'], $stockItem);
		        $stockItem->save();

				$productRepository = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
		        $product = $productRepository->get($row['cyc_sync_log_sku']);
				$product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
				$productRepository->save($product);

			}			

			//update log
			$resource->getConnection()->query("UPDATE cyc_sync_log SET cyc_sync_log_text = concat(cyc_sync_log_text, ' $override_text'), cyc_sync_log_status = 0 where cyc_sync_log_id = $id");

		}

	}

	$logger->debug('get new inventory from cyc');
	$helper = $objectManager->create('Cyclelution\Sync\Helper\Data');

	$cyc_enable = $helper->getGeneralConfig('enable');
	$cyc_userid = $helper->getGeneralConfig('cyc_userid');
	$cyc_token = $helper->getGeneralConfig('cyc_token');
	$cyc_api = $helper->getGeneralConfig('cyc_api');

	$api_export_url = "https://$cyc_api/CyclelutionWebAPI/api/CyclelutionInventory/ExportInventory";

	$postData = array(
	    'Request' => array(	'UserID'=> $cyc_userid,
	    					'SourceSystemName'=> 'WSI',
	    					'SourceSystemVersion'=> '1.0'),
	    'InventoryModelId' => 0,
	    'InvRequestType' => 0,
	    'DifferentialDateFrom' => '',
	    'IncludeRTSItemOnly' => false,
	    'DownloadLimit' => 100
	);

	$ch = curl_init($api_export_url);
	curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		//'Content-Length: ' . strlen($postData),
		'Authorization: BasicAuth ' .$cyc_token,
		'Content-Type: application/json'
		)
	);
	
	$response = json_decode(curl_exec($ch), true);

	$inventory_list = $response['Inventories'];

	if(count($inventory_list) == 0){

		$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'no inventory found'");

	} else {

		$logger->debug('loop to add inventory');
		//loop data to write quick log
		foreach($inventory_list as $row){

			$cyc_item_id = intval($row['ID']);
			$cyc_product_id = intval($row['ProductID']);
			$cyc_traveler_id = $row['TravelerID'];
			$cyc_stock = intval($row['InStock']);
			$cyc_weight = intval($row['Weight']);
			$cyc_price = floatval($row['Price']);
			$cyc_name = addslashes($row['Name']);
			$cyc_sku = "cyc-$cyc_item_id-$cyc_traveler_id-$cyc_product_id";
			//check if produc already in system via sku

			$logger->debug("checking pcheck $cyc_sku");

			$pcheck = intval($objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($cyc_sku));

			$logger->debug("pcheck $pcheck");

			//echo "productId$productId";
			//var_dump($pcheck);
			if($pcheck > 0){

				//set stock and reactivate
				$stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
				$stockItem = $stockRegistry->getStockItem($pcheck);


				$stockItem->setData('qty',$cyc_stock); //set updated quantity
				//$stockItem->setData('manage_stock',$stockData['manage_stock']);
		        $stockItem->setData('is_in_stock',1);
		        //$stockItem->setData('use_config_notify_stock_qty',1);
				//$stockRegistry->updateStockItemById($row['cyc_sync_log_sku'], $stockItem);
		        $stockItem->save();

				$productRepository = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
		        $product = $productRepository->get($cyc_sku);
				$product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
				$productRepository->save($product);

				$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_int = $pcheck, cyc_sync_log_status = 1, cyc_sync_log_sku = '$cyc_sku', cyc_sync_log_text = 'reactivated $cyc_name $cyc_item_id $cyc_item_id $cyc_product_id $cyc_traveler_id $cyc_name $cyc_price'");

				unset($product);
				unset($stockItem);
				unset($productRepository);
				unset($stockRegistry);

			} else if($pcheck == 0){

				$product = $objectManager->create('\Magento\Catalog\Model\Product');

				$product->setSku($cyc_sku); // Set your sku here
				$product->setName($cyc_name . " ($cyc_item_id)"); // Name of Product
				$product->setAttributeSetId(4); // Attribute set id
				$product->setDescription('added via cyc sync');
				$product->setCyc_item_id($cyc_item_id);
				$product->setCyc_product_id($cyc_product_id);
				$product->setCyc_traveler_id($cyc_traveler_id);
				$product->setCyc_item(1);
				$product->setStatus(1); // Status on product enabled/ disabled 1/0
				$product->setWeight($cyc_weight); // weight of product
				$product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
				$product->setTaxClassId(0); // Tax class id
				$product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
				$product->setPrice($cyc_price); // price of product
				$product->setStockData(
				                        array(
				                            'use_config_manage_stock' => 0,
				                            'manage_stock' => 1,
				                            'is_in_stock' => 1,
				                            'qty' => $cyc_stock
				                        )
				                    );

				$product->save();

				$product_id = $product->getId();

				$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_status = 1, cyc_sync_log_sku = '$cyc_sku', cyc_sync_log_int = $product_id, cyc_sync_log_text = 'insert $cyc_name $cyc_item_id $cyc_item_id $cyc_product_id $cyc_traveler_id $cyc_name $cyc_price'");

				unset($product);

			}

			

		}

		//$attributeId = $eavSetup->getAttributeId('catalog_product', 'attribute_code');

		//echo json_encode($response);

		//var_dump($response);

	}

	$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'sync complete'");

    exit;
  }
}