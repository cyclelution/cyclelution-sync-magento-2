<?php

namespace Cyclelution\Sync\Observer;

use Magento\Framework\Event\ObserverInterface;

class AfterPlaceOrder implements ObserverInterface
{
    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $order;

    public function __construct(
        \Magento\Sales\Model\Order $order,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->order = $order;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

		//$resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'AfterPlaceOrder Triggered'");

    	$this->_logger->debug("AfterPlaceOrder Triggered");

       	$orderId = $observer->getEvent()->getOrderIds();

       	$this->_logger->debug("AfterPlaceOrder OrderID ".$orderId[0]);

       	$order = $this->order->load($orderId);

       	$order_total = $order->getGrandTotal();

       	$order_date =  $order->getCreatedAt();

       	$order_notes = '';

       	$shippingAddress = $order->getShippingAddress();

       	//construct customer details
       	$CustomerCompanyName = $shippingAddress->getCompany();
       	$CustomerFirstName = $shippingAddress->getFirstname();
       	$CustomerLastName = $shippingAddress->getLastname();
       	$CustomerAddress = $shippingAddress->getStreet();
       	$CustomerCity = $shippingAddress->getCity();
       	$CustomerState = $shippingAddress->getRegionCode();
       	$CustomerZip = $shippingAddress->getPostcode();
       	$CustomerCountryID = $shippingAddress->getCountryId();
       	$CustomerPhone = $shippingAddress->getTelephone();
       	$CustomerEmail = $shippingAddress->getEmail();

       	if($CustomerCompanyName == '' || $CustomerCompanyName == null){
       		$CustomerCompanyName = $CustomerFirstName . ' ' . $CustomerLastName;
       	}

       	$this->_logger->debug("AfterPlaceOrder CustomerCompanyName$CustomerCompanyName
       	CustomerFirstName$CustomerFirstName
       	CustomerLastName$CustomerLastName
       	CustomerAddress".$CustomerAddress[0]."
       	CustomerCity$CustomerCity
       	CustomerState$CustomerState
       	CustomerZip$CustomerZip
       	CustomerCountryID$CustomerCountryID
       	CustomerPhone$CustomerPhone
       	CustomerEmail$CustomerEmail  ");

       	//$customer = $order->getCustomerId(); // using this id you can get customer name

        //get Order All Item
        $itemCollection = $order->getItemsCollection();

        $item_array = array();
        $non_item_array = array();
        $cyc_items = 0;

        foreach ($itemCollection as $item) {

        	$line = null;

		    $product_id = $item->getProductId();
		    $product_sku = $item->getSku();
		    $product_name = $item->getName();
		    $product_weight = $item->getWeight();
		    $product_qty = $item->getQtyToShip();

		    $this->_logger->debug("AfterPlaceOrder product_id$product_id product_sku$product_sku product_name$product_name ");

		    //cyc data
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);

			$cyc_item_id = $product->getCyc_item_id();
			$cyc_product_id = $product->getCyc_product_id();
			$cyc_traveler_id = $product->getCyc_traveler_id();
			$cyc_item = $product->getCyc_item();

			if($cyc_item == 1){

				$line = array(	'LotNumber'=> $cyc_traveler_id,
								'Qty' => $product_qty,
								'Weight' => $product_weight);

			    $this->_logger->debug("AfterPlaceOrder cyc_item_id$cyc_item_id cyc_product_id$cyc_product_id cyc_traveler_id$cyc_traveler_id cyc_item$cyc_item");

			    $item_array[] = $line;

			    $cyc_items = 1;

		    }

		}

		if($cyc_items == 0){

			$this->_logger->debug("AfterPlaceOrder order has no cyc items");

		} else {

	        $helper = $objectManager->create('Cyclelution\Sync\Helper\Data');
	        $cyc_userid = $helper->getGeneralConfig('cyc_userid');
			$cyc_api = $helper->getGeneralConfig('cyc_api');
			$cyc_token = $helper->getGeneralConfig('cyc_token');

			$api_export_url = "https://$cyc_api/CyclelutionWebAPI/api/CyclelutionInventory/InsertPOSOrder";

			//debug override
			//$CustomerCompanyName = 'North Hill Inc.';

	        $postData = array(
			    'Request' => array(		'UserID'=> $cyc_userid,
			    						'ClientID'=> 7),
			    'OrderHeader' => array( 'EstimateShipDate' => $order_date,
			    						'Notes' => $order_notes),
			    'OrderCompany' => array("CompanyID" => null,
			    						"CompanyName" => $CustomerCompanyName,
									    "Address" => $CustomerAddress[0],
									    "City" => $CustomerCity,
									    "StateCode" => $CustomerState,
									    "ZipCode" => $CustomerZip,
									    "ContactPerson" => $CustomerFirstName . ' ' . $CustomerLastName,
									    "Telephone" => $CustomerPhone,
									    "Cellphone" => "",
									    "Email" => $CustomerEmail,
									    "Country" => $CustomerCountryID,
									    "ExpirationDate" => null,
									    "LicensePlate" => null,
									    "PaymentTerms" => null,
									    "DriverLicense" => null),
			    'OrderItemList' => $item_array,
			    'OrderNonItemList' => $non_item_array
			);

	        $this->_logger->debug("AfterPlaceOrder Post" . json_encode($postData));

	        $ch = curl_init($api_export_url);
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				//'Content-Length: ' . strlen($postData),
				'Authorization: BasicAuth ' .$cyc_token,
				'Content-Type: application/json'
				)
			);
			
			$response = json_decode(curl_exec($ch), true);

			$this->_logger->debug("AfterPlaceOrder Result" . json_encode($response));

			if($response['Status'] == false){
				
				//failed

			} else {

				//capture order ID and store in db
				$ShipmentHeaderID = intval($response['Result']['ShipmentHeaderID']);
				$order_id = intval($orderId[0]);

				$this->_logger->debug("AfterPlaceOrder ShipmentHeaderID$ShipmentHeaderID order_id$order_id");

		        $resource->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'AfterPlaceOrder', cyc_sync_log_magento_order_id = $order_id, cyc_sync_log_cyc_order_id = $ShipmentHeaderID ");

			}

		}

        $this->_logger->debug("AfterPlaceOrder Ended");

    }

}