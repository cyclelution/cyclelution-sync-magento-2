<?php
/**
 * path:
 * magento2_location/app/code/Vendorname/Extensionname/Cron/MyCronTask.php
 */

namespace Cyclelution\Sync\Cron;

class SyncInventory
{	
	public function __construct(
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
    }

    public function execute()
    {
        /**
         * 	Do some cool stuff here
         */
        $this->_logger->debug("SyncInventory Cron Triggered");

        return $this;
    }
}