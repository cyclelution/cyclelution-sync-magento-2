<?php
 
namespace Cyclelution\Sync\Plugin;

use Magento\Framework\Exception\LocalizedException;
 
class CartPlugin
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;
    
    protected $request;
 
    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Request\Http $request,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->quote = $checkoutSession->getQuote();
        $this->request = $request;
        $this->_logger = $logger;  
    }
 
    /**
     * beforeAddProduct
     *
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     */

    /*
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {        
        $productId = (int)$this->request->getParam('product', 0);        
        $qty = (int)$this->request->getParam('qty', 1);                
        
        // do something
        // your code goes here
        $this->_logger->debug('debug1234'); 

        return [$productInfo, $requestInfo];
    }
    */
    
    /**
     * afterAddProduct
     *
     * @param      $subject
     * @param      $result    Returned value from core observed method 'addProduct'     
     */

    

    public function afterAddProduct($subject, $result)
    {
        //$quote = $result->getQuote();
        
        //MAYBE DO FUTURE QTY CHALLENGE??

        $quote = 123;
        $this->_logger->debug("afterAddProduct triggered $quote");
        // do something
        // your code goes here        
    }
    

    /*
    public function aroundAddProduct($subject, $proceed)
    {
        // do something 
        // before adding product to cart
        // your code goes here
        $productId = (int)$this->request->getParam('product', 0);        
        $qty = (int)$this->request->getParam('qty', 1);    
                
        // this will run the core addProduct function
        $returnValue = $proceed();        
        
        // below code is executed after product is added to cart
        if ($returnValue) {
            // do something
            // after adding product to cart
            // your code goes here
            
        }
        
        return $returnValue;    
    }
    */

}

