<?php
namespace Cyclelution\Sync\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	private $eavSetupFactory;

	public function __construct(
		EavSetupFactory $eavSetupFactory,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute)
	{
		$this->eavSetupFactory = $eavSetupFactory;
		$this->_eavAttribute = $eavAttribute;
	}
	
	public function install(
		ModuleDataSetupInterface $setup, 
		ModuleContextInterface $context)
	{	

		/*
		$data = [
            'cyc_sync_log_text' => "Sample title 1"
        ];

        $post = $this->_postFactory->create();
        $post->addData($data)->save();
		*/

		$setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'Install'");

		$setup->startSetup();
 
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
 
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_item_id',
			[
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'cyc item id',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '0',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => 'simple'
			]
        );

        $cyc_item_id = intval($this->_eavAttribute->getIdByCode('catalog_product', 'cyc_item_id'));

        $setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_int = $cyc_item_id,cyc_sync_log_text = 'cyc_item_id'");

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_product_id',
			[
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'cyc product id',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '0',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => 'simple'
			]
        );

        $cyc_product_id = intval($this->_eavAttribute->getIdByCode('catalog_product', 'cyc_product_id'));

        $setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_int = $cyc_product_id,cyc_sync_log_text = 'cyc_product_id'");

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_traveler_id',
			[
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'cyc traveler id',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => 'simple'
			]
        );

        $cyc_traveler_id = intval($this->_eavAttribute->getIdByCode('catalog_product', 'cyc_traveler_id'));
 
        $setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_int = $cyc_traveler_id,cyc_sync_log_text = 'cyc_traveler_id'");

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_item',
			[
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'is cyc item',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => false,
				'required' => false,
				'user_defined' => false,
				'default' => '0',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => 'simple'
			]
        );

        $cyc_item = intval($this->_eavAttribute->getIdByCode('catalog_product', 'cyc_item'));
 
        $setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_int = $cyc_item,cyc_sync_log_text = 'cyc_item'");

		$setup->getConnection()->query("INSERT INTO cyc_sync_log SET cyc_sync_log_text = 'Install done'");


       $setup->endSetup();
	}
}