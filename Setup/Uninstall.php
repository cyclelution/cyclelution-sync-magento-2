<?php
namespace Cyclelution\Sync\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Uninstall implements UninstallInterface
{	
	private $eavSetupFactory;

	public function __construct(
		EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}

	public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;

		$installer->startSetup();

		$installer->getConnection()->dropTable($installer->getTable('cyc_sync_log'));

		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

		//remove attributes
		$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_item_id');

		$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_product_id');

		$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_product_id');

		$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_traveler_id');

		$eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
			'cyc_item');

		//remove schema
		
		
		$installer->endSetup();
	}
}
