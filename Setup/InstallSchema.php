<?php
namespace Cyclelution\Sync\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('cyc_sync_log')) {

            $table = $installer->getConnection()->newTable(
                $installer->getTable('cyc_sync_log')
            )
            ->addColumn(
                'cyc_sync_log_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Log ID'
            )
            ->addColumn(
                'cyc_sync_log_text',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Name'
            )
            ->addColumn(
                'cyc_sync_log_int',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                3,
                ['default' => 0],
                'fk int'
            )
            ->addColumn(
                'cyc_sync_log_magento_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['default' => 0],
                'fk magento order int'
            )
            ->addColumn(
                'cyc_sync_log_cyc_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['default' => 0],
                'fk cyc order int'
            )
            ->addColumn(
                'cyc_sync_log_sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                [],
                'fk sku'
            )
            ->addColumn(
                'cyc_sync_log_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                1,
                ['default' => 1],
                'Sync Status'
            )
            ->addColumn(
                'cyc_sync_log_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Sync Date'
            )
            ->addColumn(
                'cyc_sync_log_json',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1000,
                [],
                'json dump'
            )
            ->setComment('Sync Log Table');
            $installer->getConnection()->createTable($table);

            
            $installer->getConnection()->addIndex(
                $installer->getTable('cyc_sync_log'),
                $setup->getIdxName(
                    $installer->getTable('cyc_sync_log'),
                    ['cyc_sync_log_sku','cyc_sync_log_text'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['cyc_sync_log_sku','cyc_sync_log_text'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
            
        }
        
        $installer->endSetup();

    }
}

